import {Text, View, TextInput, TouchableOpacity,StyleSheet} from 'react-native';
import React, {Component} from 'react';

class ParSatu extends Component {
  constructor(props){
    super(props);

    this.state = {
      warnaText: 'blue',
      text: '',
      text1: '',
      fulltext : '',
    };
  }

  getInputSatu = text => {
    this.setState({text: text});
   // console.log(this.state.text);
  };

  getInputDua = text => {
    this.setState({text1: text});
    //console.log(this.state.text);
  };

  ubahwarna = () => {
    const warna = this.state.warnaText;
    if (warna === 'blue'){
      this.setState({warnaText: 'green'});
    } else if (warna === 'green') {
      this.setState({warnaText: 'red'});
    } else {
      this.setState({warnaText: 'blue'});
    }
  }

    updateFullText = () => {
      let text = this.state.text;
      let text1 = this.state.text1;
      this.setState({fulltext: text + '' + text1});
    
  };

  render () {
    return (
      <View style={style.component}> 
      <Text style={{color: this.state.warnaText}}> Halo React Native </Text>
      <TextInput 
      onChangeText={text => this.getInputSatu(text)} 
      style={style.input} />
      <TextInput 
      onChangeText={text => this.getInputDua(text)}
      style={style.input} />
      <Text>fulltext : {this.state.fulltext}</Text>
      <TouchableOpacity  
      onPress={() => this.updateFullText()} 
      style={style.btn}>
      <Text>Klik</Text>
      </TouchableOpacity>
      
      </View>
    );
  }
}
export default ParSatu;

const style= StyleSheet.create({
  component: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btn: {
    backgroundColor: 'yellow',
    padding: 5,
  },
  input: {
    backgroundColor: 'orange',
    margin: 2,
    width: '100%',
    paddingHorizontal:5,
    
  }
});
