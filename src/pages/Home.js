import {Text, View, TextInput, TouchableOpacity,StyleSheet,Image, ImageBackground} from 'react-native';
import React, {Component} from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import  PartDua from './PartDua';
import PartTiga from './PartTiga'


class Home extends Component {
  constructor(props){
    super(props);

    this.state = {
      warnaText: 'blue',
      text: '',
      text1: '',
      fulltext : '',
    };
  }

  getInputSatu = text => {
    this.setState({text: text});
   // console.log(this.state.text);
  };

  getInputDua = text => {
    this.setState({text1: text});
    //console.log(this.state.text);
  };

  ubahwarna = () => {
    const warna = this.state.warnaText;
    if (warna === 'blue'){
      this.setState({warnaText: 'green'});
    } else if (warna === 'green') {
      this.setState({warnaText: 'red'});
    } else {
      this.setState({warnaText: 'blue'});
    }
  }

    updateFullText = () => {
      let text = this.state.text;
      let text1 = this.state.text1;
      this.setState({fulltext: text + '' + text1});
    
  };

  render () {
    return (
      <View style={style.component}> 
      <Text style={{color: this.state.warnaText}}> Halo React Native </Text>
      <TextInput 
      onChangeText={text => this.getInputSatu(text)} 
      style={style.input} />
      <TextInput 
      onChangeText={text => this.getInputDua(text)}
      style={style.input} />
      <Text>fulltext : {this.state.fulltext}</Text>
      <TouchableOpacity  
      onPress={() => this.updateFullText()} 
      style={style.btn}>
      <Text>Klik</Text>
      </TouchableOpacity>
      
      </View>
    );
  }
}

const Tab = createBottomTabNavigator();

export default function App() {
  return (
      <Tab.Navigator
      initialRouteName='Home'
      screenOptions={{headerShown: false}}>
        <Tab.Screen name="HOME" component={Home} options= {{
          tabBarIcon: ({focused}) => (
            <View>
              <Image source={require('../Image/OIP.jpg')}
              resizeMode="contain"
              style= {{
                width:32,
                height:32,
                marginTop:17,
              }}/>
            </View>
          )
        }}/>
        <Tab.Screen name="RESEP MAKANAN" component={PartDua} options= {{
          tabBarIcon: ({focused}) => (
            <View>
              <Image source={require('../Image/tt.png')}
              resizeMode="contain"
              style={{
                width: 35,
                height:35,
                marginTop:4,
              }} />
            </View>
          )
        }}/>
        <Tab.Screen name="NOTIFIKASI" component={PartTiga} options={{
          tabBarIcon: ({focused}) => (
            <View>
              <Image source={require('../Image/bb.jpg')}
              resizeMode='contain'
              style={{
                width:25,
                height:25,
                marginTop:18
              }}/>
            </View>
          )
        }}/>
      </Tab.Navigator>
  );
}

const style= StyleSheet.create({
  component: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btn: {
    backgroundColor: 'yellow',
    padding: 5,
  },
  input: {
    backgroundColor: 'gray',
    margin: 2,
    width: '100%',
    paddingHorizontal:5,
  },
});
